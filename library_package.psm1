# This file is part of the XVM Framework project.
#
# Copyright (c) 2017-2022 XVM Team.
#
# XVM Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XVM Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#



function Create-WotMeta($PackageConfig, $FilePath){
    $content =
"<root>
    <id>$($PackageConfig.wot_id)</id>
    <name>$($PackageConfig.wot_name)</name>
    <description>$($PackageConfig.wot_description)</description>
    <version>$($PackageConfig.version)</version>
</root>
"

    Out-File -FilePath $FilePath -Encoding utf8 -InputObject $content
}


function Create-XfwMeta($PackageConfig, $ComponentConfig, $FilePath){

    $result = @{
        id = $ComponentConfig.xfw_id
        name = $ComponentConfig.xfw_name
        description = $ComponentConfig.xfw_description
        version = $PackageConfig.version
        dependencies = $ComponentConfig.xfw_dependencies
        features = $ComponentConfig.xfw_features
        features_provide = $ComponentConfig.xfw_features_provide
        url = $ComponentConfig.xfw_url
        url_update = $ComponentConfig.xfw_url_update
        wot_versions = $ComponentConfig.xfw_wot_versions
        wot_versions_strategy = $ComponentConfig.xfw_wot_versions_strategy
        architecture = $ComponentConfig.xfw_architecture
    }

    ConvertTo-Json $result | Set-Content -Path $FilePath
}


function Build-PackageComponent($PackageContext, $ComponentName) {
    Write-Output "  - component: $comp_name"
    $pkg_context = $PackageContext
    $comp_path = $(Join-Path -Path $pkg_context.path_pkg -ChildPath $ComponentName -Resolve) -replace "\\","/"

    # environment
    $comp_config = $(Get-Content -Path  "$comp_path/_meta/component.json" | ConvertFrom-Json)
    $comp_context = @{
        config = $comp_config

        path_comp = $comp_path

        path_out_xfw = Join-Path -Path $pkg_context.path_out_wotmod -ChildPath "res/mods/xfw_packages/$($comp_config.xfw_prefix)/"
    }

    #build, native
    foreach($native_path_src in $(Get-ChildItem -Path $comp_context.path_comp -Filter "native*" -Directory)){
        Write-Output "    - native: $native_path_src"
        $native_config = $(Get-Content -Path  "$native_path_src/_meta/native.json" | ConvertFrom-Json)

        #build
        foreach($native_arch in $native_config.build_architectures){
            Write-Output "      - $native_arch"

            $native_path_tgt     = Join-Path -Path $pkg_context.path_out_build -ChildPath "${component_name}_build/"
            $native_path_install = Join-Path -Path $pkg_context.path_out_build -ChildPath "${component_name}_install/"

            #build, perform build
            #TODO: add ability to configure prefix
            Build-CmakeProject `
                -SourceDirectory   "$native_path_src"`
                -BuildDirectory    "$native_path_tgt"`
                -InstallDirectory  "$native_path_install"`
                -Arch              "$native_arch"`
                -Defines            $native_config.build_defines

            #copy
            Copy-Item -Path "$native_path_install/*" -Destination $pkg_context.path_out_devel -Recurse -Force
            if($native_config.install_xfw){
                New-Item -Path "$($comp_context.path_out_xfw)/$native_arch/" -ItemType Directory -Force -ErrorAction SilentlyContinue
                Copy-Item -Path "$native_path_install/$native_arch/bin/*.dll" -Destination "$($comp_context.path_out_xfw)/$native_arch/" -Force
                Copy-Item -Path "$native_path_install/$native_arch/bin/*.pyd" -Destination "$($comp_context.path_out_xfw)/$native_arch/" -Force
            }
        }

    }

    #build, python
    #TODO: add support for other output dir
    foreach($comp_python_dirname in $(Get-ChildItem -Path $comp_context.path_comp -Filter "python*" -Directory)){
        Write-Output "    - python: $comp_python_dirname"
        
        $comp_python_outpath = Join-Path -Path $comp_context.path_out_xfw -ChildPath "python/" 
        Build-PythonDirectory -FileDirectory $comp_python_dirname -OutputDirectory $comp_python_outpath
    }

    #create, python empty
        if($(Test-Path $(Join-Path -Path $comp_context.path_out_xfw -ChildPath "python/") -PathType Container) -eq $true -and
        $(Test-Path $(Join-Path -Path $comp_context.path_out_xfw -ChildPath "__init__.pyc") -PathType Leaf) -eq $false){
         $temp_path_in = Join-Path $comp_context.path_out_xfw -ChildPath  "__init__.py"
         New-Item -Path $temp_path_in -ItemType File
         Build-PythonFile -FilePath $temp_path_in -OutputDirectory $comp_context.path_out_xfw
         Remove-Item -Path $temp_path_in
     }

    #create, xfw meta
    Create-XfwMeta -PackageConfig $pkg_context.config -ComponentConfig $comp_config -FilePath "$($comp_context.path_out_xfw)/xfw_package.json"`
}

function Build-Package($PackageDirectory, $OutputDirectory) {
    #prepare
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$OutputDirectory/" | Out-Null
    $PackageDirectory = $(Resolve-Path -Path $PackageDirectory) -replace "\\", "/"
    $OutputDirectory  = $(Resolve-Path -Path $OutputDirectory)  -replace "\\", "/"

    # config
    $pkg_config = $(Get-Content -Path  "$PackageDirectory/_meta/package.json" | ConvertFrom-Json)
    $pkg_context = @{
        config = $pkg_config

        path_license             = Resolve-Path -Path $(Join-Path -Path $PackageDirectory -ChildPath $pkg_config.build_license)
        path_pkg                 = $PackageDirectory

        path_out                  = $OutputDirectory
        path_out_build            = Join-Path -Path $OutputDirectory -ChildPath "build/"
        path_out_deploy           = Join-Path -Path $OutputDirectory -ChildPath "deploy/"
        path_out_wotmod           = Join-Path -Path $OutputDirectory -ChildPath "wotmod/"
        path_out_devel            = Join-Path -Path $OutputDirectory -ChildPath "devel/"
    }

    Write-Output "- package: $($pkg_context.config.id)"

    # prepare, directories
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$($pkg_context.path_out)/"        | Out-Null
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$($pkg_context.path_out_build)/"  | Out-Null
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$($pkg_context.path_out_deploy)/" | Out-Null
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$($pkg_context.path_out_wotmod)/" | Out-Null
    New-Item -ItemType Directory -ErrorAction SilentlyContinue -Path "$($pkg_context.path_out_devel)/"  | Out-Null
    
    # process, components
    foreach ($comp_name in $pkg_config.build_components) {
        Build-PackageComponent -PackageContext $pkg_context -ComponentName $comp_name
    }

    # create, license
    Copy-Item -Path $pkg_context.path_license -Destination "$($pkg_context.path_out_wotmod)/LICENSE.md"

    # create, meta
    Create-WotMeta -PackageConfig $pkg_config -FilePath "$($pkg_context.path_out_wotmod)/meta.xml"

    # pack, wotmod
    Create-Zip -Directory $pkg_context.path_out_wotmod
    Move-Item "$($pkg_context.path_out_wotmod)/output.zip" "$($pkg_context.path_out_deploy)/$($pkg_config.wot_id)_$($pkg_config.version).wotmod" -Force -ErrorAction SilentlyContinue

    # pack, devel
    Create-Zip -Directory $pkg_context.path_out_devel -CompressionLevel 9
    Move-Item "$($pkg_context.path_out_devel)/output.zip" "$($pkg_context.path_out_deploy)/$($pkg_config.wot_id)_$($pkg_config.version)-devel.zip" -Force -ErrorAction SilentlyContinue
}
