# This file is part of the XVM Framework project.
#
# Copyright (c) 2017-2022 XVM Team.
#
# XVM Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XVM Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

$xvm_pslib_version="2022.08.07"

Import-Module $(Join-Path -Path $PSScriptRoot -ChildPath "library_common.psm1")    -Force -DisableNameChecking
Import-Module $(Join-Path -Path $PSScriptRoot -ChildPath "library_as3.psm1")       -Force -DisableNameChecking
Import-Module $(Join-Path -Path $PSScriptRoot -ChildPath "library_hashtable.psm1") -Force -DisableNameChecking
Import-Module $(Join-Path -Path $PSScriptRoot -ChildPath "library_native.psm1")    -Force -DisableNameChecking
Import-Module $(Join-Path -Path $PSScriptRoot -ChildPath "library_package.psm1")   -Force -DisableNameChecking
Import-Module $(Join-Path -Path $PSScriptRoot -ChildPath "library_python.psm1")    -Force -DisableNameChecking
Import-Module $(Join-Path -Path $PSScriptRoot -ChildPath "library_sign.psm1")      -Force -DisableNameChecking
Import-Module $(Join-Path -Path $PSScriptRoot -ChildPath "library_vcs.psm1")       -Force -DisableNameChecking
Import-Module $(Join-Path -Path $PSScriptRoot -ChildPath "library_zip.psm1")       -Force -DisableNameChecking



function Download-NativeDevelPackage($OutputPath)
{
    $link = (Invoke-WebRequest "https://gitlab.com/xvm/xfw/xfw.native/-/raw/master/_latest_devel.txt" -UseBasicParsing).Content
    Invoke-WebRequest "$link" -OutFile devel.zip
    Expand-Archive -Path ./devel.zip -DestinationPath "$OutputPath"
    Remove-Item -Path "./devel.zip"
}


function Find-Java([Switch] $Required)
{
    return Find-Application "java" -Required:$Required
}


function Find-Mono([Switch] $Required)
{
    if(${env:XVMBUILD_MONO_FILEPATH})
    {
        if(Find-Application ${env:XVMBUILD_MONO_FILEPATH})
        {
            return $true
        }
    }

    $os = $(Get-OS)

    if($os -eq "windows")
    {
        return $false
    }

    $path = Find-Application "mono" -Required:$Required
    if(!$path)
    {
        return $false
    }

    ${env:XVMBUILD_MONO_FILEPATH} = $path

    return $path
}


function Find-Patch([Switch] $Required)
{
    $patchProgram =  Find-Application "patch" -Required:$false

    if(!$patchProgram)
    {
        $os = Get-OS
        if($os -eq "windows")
        {
            $patchDir="$PSScriptRoot/bin/${os}_i686/patch"
        }
        else
        {
            $patchDir="$PSScriptRoot/bin/${os}_${Get-Architecture}/patch"
        }

        Edit-Path -Path "${patchDir}" -Prepend

        $patchProgram = Find-Application "patch" -Required:$Required
    }
    return $patchProgram
}


function Find-Wget([Switch] $Required)
{
    return Find-Application "wget" -Required:$Required
}


function Find-SentryCli([Switch] $Required)
{
    $sentryCliProgram =  Find-Application "sentry-cli" -Required:$false

    if(!$sentryCliProgram)
    {
        $os = Get-OS
        $arch = Get-Architecture
        $cliDir="$PSScriptRoot/bin/${os}_${arch}/sentry-cli"

        Edit-Path -Path "${cliDir}" -Prepend

        $sentryCliProgram = Find-Application "sentry-cli" -Required:$Required
    }

    return $sentryCliProgram
}


function Upload-Symbols($Directory){
    Find-SentryCli -Required | Out-Null

    $token = ${env:XFW_SENTRY_AUTHTOKEN}
    $org = ${env:XFW_SENTRY_ORGANIZATION}
    $project = ${env:XFW_SENTRY_PROJECT}
    $url = ${env:XFW_SENTRY_URL}

    if(($null -eq $token) -or ($null -eq $org) -or ($null -eq $project)){
        return
    }

    sentry-cli --url $url --auth-token $token upload-dif --org $org --project $project "$Directory"
}
