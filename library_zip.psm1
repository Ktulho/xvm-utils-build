# This file is part of the XVM Framework project.
#
# Copyright (c) 2017-2022 XVM Team.
#
# XVM Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XVM Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#



function Find-Unzip([Switch] $Required)
{
    return Find-Application "unzip" -Required:$Required
}

function Find-Zip([Switch] $Required)
{
    $zipProgram =  Find-Application "zip" -Required:$false

    if(!$zipProgram)
    {
        $os = Get-OS
        $arch = Get-Architecture
        if($os -eq "windows")
        {
            $zipDir="$PSScriptRoot/bin/${os}_i686/zip"
        }
        else
        {
            $zipDir="$PSScriptRoot/bin/${os}_${arch}/zip"
        }

        Edit-Path -Path "${zipDir}" -Prepend

        $zipProgram = Find-Application "zip" -Required:$Required
    }
    return $zipProgram
}

function Create-Zip($Directory, $CompressionLevel=0)
{
    Push-Location $Directory

    Find-Zip -Required | Out-Null
    zip -$CompressionLevel -q -X -r ./output.zip ./*

    Pop-Location
}