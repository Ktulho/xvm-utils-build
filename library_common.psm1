# This file is part of the XVM Framework project.
#
# Copyright (c) 2017-2022 XVM Team.
#
# XVM Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XVM Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#


function Get-Architecture()
{
    $os = Get-OS

    if($os -eq "windows")
    {
        switch(${env:PROCESSOR_ARCHITECTURE})
        {
            AMD64
            {
                return "amd64"
            }

            x86
            {
                return "i686"
            }
        }
    }
    elseif($os -eq "linux")
    {
        switch ($(uname -m))
        {
            x86_64
            {
                return "amd64"
            }
        }
    }

    return "Unknown"
}


function Get-OS()
{
    if($PSVersionTable.PSEdition -ne "Core")
    {
        return "windows"
    }

    if($PSVersionTable.OS.StartsWith("Microsoft"))
    {
        return "windows"
    }

    if($PSVersionTable.OS.StartsWith("Linux"))
    {
        return "linux"
    }

    return "unknown"
}


function Edit-Path ($Path, [switch] $Append, [switch] $Prepend)
{
    $os = Get-OS


    if($Append)
    {
        if($os -eq "windows")
        {
            ${env:PATH}="${env:PATH};${Path}"
        }
        else
        {
            ${env:PATH}="${env:PATH}:${Path}"
        }
    }

    if($Prepend)
    {
        if($os -eq "windows")
        {
            ${env:PATH}="${Path};${env:PATH}"
        }
        else
        {
            ${env:PATH}="${Path}:${env:PATH}"
        }
    }

}



function Find-Application([string] $Command, [Switch] $Required)
{
    $cmd = Get-Command -Name $Command -ErrorAction SilentlyContinue

    if($cmd)
    {
        return $cmd.Path
    }

    if($Required)
    {
        Write-Error -Message "$($Command) is required"
        exit 1
    }

    return $false
}

function Write-ErrorLine($message) {
    [Console]::ForegroundColor = 'red'
    [Console]::Error.WriteLine($message)
    [Console]::ResetColor()
}

