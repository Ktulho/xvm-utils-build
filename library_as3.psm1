# This file is part of the XVM Framework project.
#
# Copyright (c) 2017-2022 XVM Team.
#
# XVM Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XVM Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

function Build-AS3Proj($Project)
{
    if(${env:XVMBUILD_FDBUILD_FILEPATH} -eq $null)
    {
        Find-FDBuild -Required
    }

    if($(Get-OS) -eq "windows")
    {
        Invoke-Expression -Command "${env:XVMBUILD_FDBUILD_FILEPATH} -notrace -compiler:'${env:FLEX_HOME}' -cp:'' '$Project'"
    }
    else
    {
        if(${env:XVMBUILD_MONO_FILEPATH} -eq $null)
        {
            Find-Mono -Required | Out-Null
        }
        Invoke-Expression -Command "${env:XVMBUILD_MONO_FILEPATH} `"${env:XVMBUILD_FDBUILD_FILEPATH}`" -notrace -compiler:`"${env:FLEX_HOME}`" -cp:`"`" `"$Project`""
    }

    if($LASTEXITCODE -ne 0)
    {
        exit 1
    }
}

function Find-FDBuild([Switch] $Required)
{
    if(${env:XVMBUILD_FDBUILD_FILEPATH})
    {
        $path = Find-Application ${env:XVMBUILD_FDBUILD_FILEPATH} -Required:$false
    }

    if(!$path)
    {
        $path = Find-Application "fdbuild" -Required:$false
    }

    if(!$path)
    {
        Edit-Path -Path "${PSScriptRoot}/bin/msil/fdbuild/" -Prepend
        $path = Find-Application "fdbuild.exe" -Required:$Required
    }

    ${env:XVMBUILD_FDBUILD_FILEPATH} = $path
    return $path
}

function Find-Flex([Switch] $Required)
{
    $playerVersions = ("11.0", "11.1")
    $os = Get-OS
    $java = Find-Java -Required:$Required

    if($java -eq $null)
    {
        return $false
    }

    #flex_home
    if(${env:FLEX_HOME} -eq $null)
    {
        if($os -eq "windows")
        {
            ${env:FLEX_HOME}="${env:LOCALAPPDATA}/FlashDevelop/Apps/flexsdk/4.6.0"
        }
        else {
            ${env:FLEX_HOME}="/opt/apache-flex"
        }
    }

    if(!$(Test-Path ${env:FLEX_HOME}))
    {
        if($Required)
        {
            Write-Error "Apache Flex directory is not found"
            exit 1
        }
        return $false
    }

    Edit-Path -Path "${env:FLEX_HOME}/bin/" -Append

    #compc
    if($os -eq "windows")
    {
        ${env:XVMBUILD_COMPC_FILEPATH}=Join-Path "${env:FLEX_HOME}" "bin/compc.bat"
    }
    else
    {
        ${env:XVMBUILD_COMPC_FILEPATH}=Join-Path "${env:FLEX_HOME}" "bin/compc"
    }
    if(!$(Test-Path ${env:XVMBUILD_COMPC_FILEPATH}))
    {
        if($Required)
        {
            Write-Error "Apache Flex compc file is not found"
            exit 1
        }
        return $false
    }

    #playerglobal
    if(${env:PLAYERGLOBAL_HOME} -eq $null)
    {
        ${env:PLAYERGLOBAL_HOME} = Join-Path "${env:FLEX_HOME}" "frameworks/libs/player/"
    }

    foreach($playerVersion in $playerVersions)
    {
        $filepath = Join-Path "${env:PLAYERGLOBAL_HOME}" "${playerVersion}"
        $filepath = Join-Path "${filepath}" "playerglobal.swc"
        if(!(Test-Path $filepath))
        {
            New-Item -ItemType Directory -Path $(Split-Path -Parent $filepath) -ErrorAction SilentlyContinue

            Invoke-WebRequest -Uri "https://github.com/nexussays/playerglobal/raw/master/${playerVersion}/playerglobal.swc"  -OutFile $filepath
        }
    }

    return ${env:FLEX_HOME}
}

function Find-Mtasc([Switch] $Required)
{
    return Find-Application "mtasc" -Required:$Required
}

function Find-Rabcdasm([Switch] $Required)
{
    $path =  Find-Application "rabcdasm" -Required:$false
    if(!$path)
    {
        $os = Get-OS
        if($os -eq "windows")
        {
            $rabcdasmDir="$PSScriptRoot/bin/${os}_i686/swf-disasm"
        }
        else
        {
            $rabcdasmDir="$PSScriptRoot/bin/${os}_$(Get-Architecture)/swf-disasm"
        }

        Edit-Path -Path "${rabcdasmDir}" -Prepend

        $path = Find-Application "rabcdasm" -Required:$Required
    }

    return $path
}