# This file is part of the XVM Framework project.
#
# Copyright (c) 2017-2022 XVM Team.
#
# XVM Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XVM Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#



function Build-PythonFile($FilePath, $OutputDirectory, $OutputFileName = $null, [Switch] $UseHashTable)
{
    $python = Find-Python -Required

    $relativePath = $(Resolve-Path -Path $FilePath -Relative).Replace(".\","")

    if($UseHashTable -eq $true)
    {
        if(Test-Path "$OutputDirectory/")
        {
            if(!$hashtable.FileNeedUpdate($relativePath,$relativePath))
            {
                continue
            }
        }
    }

    Write-Output "  * ${relativePath}"

    & $python.Path -m py_compile "${FilePath}" | Out-Null

    if($(Test-Path "${OutputDirectory}") -eq $false)
    {
        New-Item -ItemType Directory "${OutputDirectory}" | Out-Null
    }

    Move-Item "${FilePath}c" "${OutputDirectory}" -Force

    if ($OutputFileName -ne $null) {
        $filename = Split-Path "${FilePath}c" -Leaf
        Move-Item (Join-Path "${OutputDirectory}" "${filename}") (Join-Path "${OutputDirectory}" "${OutputFileName}")
    }


    if($UseHashTable -eq $true)
    {
        $hashtable.UpdateFileHash($relativePath,$relativePath)
    }
}


function Build-PythonDirectory($FileDirectory, $OutputDirectory, [Switch] $UseHashTable)
{
    $dir_in = Resolve-Path $FileDirectory

    New-Item -Path $OutputDirectory -ItemType Directory -Force -ErrorAction SilentlyContinue
    $dir_out = Resolve-Path $OutputDirectory

    Push-Location -Path $dir_in

    foreach($file in $(Get-ChildItem -Path $dir_in -Filter "*.py" -Recurse))
    {
        $refpath = Resolve-Path $file.FullName -Relative
        $outdir = Split-Path $(Join-Path -Path $dir_out -ChildPath $refpath) -Parent

        Build-PythonFile -FilePath $file.FullName -OutputDirectory $outdir
    }

    Pop-Location
}


function Find-Python([Switch] $Required)
{
    $appNames = @(
        "python2.7",
        "python2",

        "C:\Program Files\Python27\python.exe",,
        "C:\Program Files (x86)\Python27\python.exe",

        "C:\Python27\python.exe",,
        "C:\Python27_32\python.exe",
        "C:\Python27_64\python.exe",

        "C:\Python\27\python.exe"
        "C:\Python\27_32\python.exe",
        "C:\Python\27_64\python.exe",

        "C:\Software\Python27\python.exe",
        "C:\Software\Python27_32\python.exe",
        "C:\Software\Python27_64\python.exe"
        
        "C:\Software\Python\27\python.exe"
        "C:\Software\Python\27_32\python.exe",
        "C:\Software\Python\27_64\python.exe",

        "python"
    )

    $path = $null;

    foreach ($appname in $appNames) {
        $path = Find-Application $appname

        if(!$path){
            continue;
        }

        #Fix Windows 19.03 Windows Store install invitation
        if((Get-Item -Path $path).Length -eq 0){
            $path = $null
        }

        if($null -ne $path){
            break;
        }
    }

    if(!$path)
    {
        if($Required)
        {
            Write-Error -Message "Python is required"
            exit 1
        }
        return $false
    }

    $path_expr = $path -replace " ", "`` "
    $version = (Invoke-Expression "$path_expr -c 'import sys; print(sys.version)'")

    if ($null -eq $version){
        Write-Error -Message "Failed to run Python Interpreter"
        exit 1
    }

    $version = $version -replace "Python ","" 
    if(!$version.StartsWith("2.7"))
    {
        Write-Error -Message "Python 2.7 is required, current version: ${version}"
        exit 1
    }

    $directory = Split-Path -Path $path

    Edit-Path -Path "${directory}" -Prepend

    return @{
        "Path" = $path
        "Directory" = $directory
        "Version" = $version
    }
}
