# This file is part of the XVM Framework project.
#
# Copyright (c) 2017-2022 XVM Team.
#
# XVM Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XVM Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#



function Get-FileHashTable($JsonPath)
{
    return [FileHashTable]::new($JsonPath)
}

class FileHashTable
{
    [String] $JsonFile = ""
    [hashtable] $Hashes = @{}


    FileHashTable([string] $JsonPath)
    {
        $this.JsonFile = $JsonPath
        $this.LoadJson()
    }

    LoadJson()
    {
        if(Test-Path -Path $this.JsonFile )
        {
            $records  = $(Get-Content -Path $this.JsonFile | ConvertFrom-Json).psobject.properties
            foreach($record in $records)
            {
                $this.Hashes.Add($record.Name,$record.Value)
            }
        }
    }

    SaveJson()
    {
        $dirPath = Split-Path -Parent $this.JsonFile
        if($(Test-Path "${dirPath}") -eq $false)
        {
            New-Item -ItemType Directory "$dirPath"
        }
        ConvertTo-Json $this.Hashes | Set-Content -Path $this.JsonFile
    }

    [bool] FileNeedUpdate($RelativePath, $FileLocation)
    {
        if($this.Hashes.ContainsKey($RelativePath))
        {
            $oldfile_hash = $this.Hashes.Get_Item($RelativePath)
            $file_hash = $(Get-FileHash -Path $FileLocation -Algorithm SHA256).Hash

            if($oldfile_hash -eq $file_hash)
            {
                return $false
            }
        }

        return $true
    }

    UpdateFileHash($RelativePath, $FileLocation)
    {
        $value =  $(Get-FileHash -Path $FileLocation -Algorithm SHA256).Hash
        if($this.Hashes.ContainsKey($RelativePath))
        {
            $this.Hashes[$RelativePath]=$value
        }
        else
        {
            $this.Hashes.Add($RelativePath,$value)
        }
    }
}