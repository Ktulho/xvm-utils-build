# This file is part of the XVM Framework project.
#
# Copyright (c) 2017-2022 XVM Team.
#
# XVM Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XVM Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

function Get-MercurialRepoStats($Path)
{
    Find-Mercurial -Required

    if($Path -eq $null)
    {
        $Path = $PWD.Path
    }

    #shitty hack: add leading zeros to rev to fix WoT version comparison algorithm
    $rev = $(hg parent --template "{rev}")
    $rev_zerocount = 5 - $rev.Length
    while ($rev_zerocount -gt 0) {
        $rev = "0" + $rev
        $rev_zerocount --
    }

    return  @{
        "Author"      = $(hg parent --template "{author}")
        "Branch"      = $(hg parent --template "{branch}")
        "Date"        = $(hg parent --template "{date|isodate}")
        "Description" = $(hg parent --template "{desc}")
        "Hash"        = $(hg parent --template "{node|short}")
        "Revision"    = $rev
        "Tags"        = $(hg parent --template "{tags}")
    }
}

function Find-Git([Switch] $Required)
{
    return Find-Application "git" -Required:$Required
}

function Find-Mercurial([Switch] $Required)
{
    return Find-Application "hg" -Required:$Required
}
