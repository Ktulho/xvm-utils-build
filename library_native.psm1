# This file is part of the XVM Framework project.
#
# Copyright (c) 2017-2022 XVM Team.
#
# XVM Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XVM Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

function Build-CmakeProject
{
    param (
        [string]$SourceDirectory,
        [string]$BuildDirectory,
        [string]$InstallDirectory,
        [string]$PrefixDirectory = "",
        [string]$ToolchainFile,
        [string]$Config = "RelWithDebInfo",
        [string]$Generator = "",
        [string]$Toolchain = "",
        [string]$Arch = "",
        [string[]]$Defines = $null
    )

    Write-Output "Building CMake Project: $SourceDirectory"

    #fill prefix directory
    if (!$PrefixDirectory){
        $PrefixDirectory = $InstallDirectory.Replace('\','/').TrimEnd('/') + "/${Arch}/"
    }
    elseif($PrefixDirectory -is [array]){
        $PrefixDirectory = $PrefixDirectory.Replace('\','/').TrimEnd('/') -join "/${Arch}/;"
    }
    else{
        $PrefixDirectory = $PrefixDirectory.Replace('\','/').TrimEnd('/') + "/${Arch}/"
    }

    #fix toolchain file
    if ($ToolchainFile){
        $ToolchainFile = $ToolchainFile.Replace('\','/')
    }

    #fix build directory
    $BuildDirectory = "$BuildDirectory/$Arch/"
    $InstallDirectory = "$InstallDirectory/$Arch/"

    #create directories
    Remove-Item -Recurse -Path $BuildDirectory -ErrorAction SilentlyContinue | Out-Null
    New-Item -ItemType Directory -Path $BuildDirectory -ErrorAction SilentlyContinue | Out-Null

    #1. generator
    if ($Generator){
        $Generator = "-G " + $Generator
    }
    else{
        $Generator = ""
    }

    #2. Toolchain
    if($Toolchain){
        $Toolchain = "-T " + $Toolchain
    }
    else {
        $Toolchain = ""
    }

    #3. Arch
    if($Arch){
        if($Arch -eq "x86_32"){
            $Arch = "Win32"
        }
        elseif($arch -eq "x86_64"){
            $Arch = "x64"
        }
        $Arch = "-A " + $Arch
    }
    else {
        $Arch = ""
    }


    #generate msvs proj
    cmake $Generator $Toolchain $Arch -S"$SourceDirectory" -B"$BuildDirectory" -DCMAKE_INSTALL_PREFIX="$InstallDirectory" -DCMAKE_BUILD_TYPE="$Config" -DCMAKE_PREFIX_PATH="$PrefixDirectory" -DCMAKE_TOOLCHAIN_FILE="$ToolchainFile" $Defines
    if ($LastExitCode -ne 0) {
        exit $LastExitCode
    }

    #build proj
    cmake --build "$BuildDirectory" --target INSTALL --config $Config
    if ($LastExitCode -ne 0) {
        exit $LastExitCode
    }
}
